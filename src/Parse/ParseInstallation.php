<?php

namespace DIParse;

/**
 * ParseInstallation - Representation of an Installation stored on Parse.
 *
 * @author Fosco Marotto <fjm@fb.com>
 */
class ParseInstallation extends ParseObject
{
    public static $parseClassName = '_Installation';
}
