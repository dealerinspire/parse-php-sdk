<?php
namespace DIParse;

use Illuminate\Support\ServiceProvider;

/**
 * @package App\Providers
 */
class Provider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $config = $this->app->config->get('services.sashido');

        ParseClient::initialize($config['app_id'], $config['rest_key'], $config['master_key']);
        ParseClient::setServerURL('https://pg-app-cq3gcbdxwxyvbx21d8ha0dg2udxb6w.scalabl.cloud', '1');
        ParseClient::setConnectionTimeout(3);
        ParseClient::setTimeout(3);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}


