<?php

namespace DIParse\Test;

use DIParse\ParseClient;
use DIParse\ParseObject;
use DIParse\ParseQuery;

class Helper
{
    public static function setUp()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        date_default_timezone_set('UTC');

        ParseClient::initialize(
            'myAppId',
            'rest-api-key-here',
            'myMasterKey',
            true,
            'account-key-here'
        );
        ParseClient::setServerURL('http://192.168.10.10:1337/parse', '/');
    }

    public static function tearDown()
    {
    }

    public static function clearClass($class)
    {
        $query = new ParseQuery($class);
        $query->each(
            function (ParseObject $obj) {
                $obj->destroy(true);
            },
            true
        );
    }
}
