<?php

namespace DIParse\Test;

use DIParse\ParseConfig;

class ConfigMock extends ParseConfig
{
    public function __construct()
    {
        $this->setConfig(['foo' => 'bar', 'some' => 1]);
    }
}
